#!/bin/bash

url_path="http://skuld.cs.umass.edu/traces/storage/"
files="Financial1.spc Financial2.spc WebSearch1.spc WebSearch2.spc WebSearch3.spc"

mkdir -p trace
cd trace

for file in $files; do
    wget ${url_path}/${file}.bz2
done

bunzip2 *
