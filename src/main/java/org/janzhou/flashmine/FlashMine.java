package org.janzhou.flashmine;

import java.io.*;
import java.util.*;

import org.janzhou.flashmine.*;

/**
 * Created by Jian Zhou on 5/25/15.
 * Home Page: http:janzhou.org
 */

public class FlashMine {
    private static final int MapEntryPerPage = 1024;
    private static final int PageSize = 4*1024;

    public FlashMine(String[] args){
    }

    public static void main(String [] args) {
        List<Partition> partitions = Partition.fromFile(args[0],
                Integer.parseInt(args[1]));

        //int dirtyRead = 0, dirty = 0;

        for(Iterator<Partition> i = partitions.iterator(); i.hasNext(); ) {
            Partition partition= i.next();
            partition.readwrite();
            //dirty += partition.getDirty();
            //dirtyRead += partition.getDirtyRead();
            System.out.printf("%d\t%d\n", partition.getDirty(), partition.getDirtyRead());
        }

        //System.out.println(dirty);
        //System.out.println(dirtyRead);
    }
}
