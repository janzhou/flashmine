package org.janzhou.flashmine;

import java.io.*;
import java.util.*;

import org.janzhou.flashmine.*;

/**
 * Created by Jian Zhou on 5/26/15.
 * Home Page: http:janzhou.org
 */

public class ReadWrite {
    private static final int MapEntryPerPage = 1024;
    private static final int PageSize = 4*1024;

    public static void main(String [] args) {
        if(args.length < 2) {
            System.out.print("Param: <Trace File> <Partition Size>");
            return;
        }
        List<Partition> partitions = Partition.fromFile(args[0],
                Integer.parseInt(args[1]));

        int dirtyRead = 0, dirty = 0;
        int num_read = 0, num_write = 0;

        System.out.print("dirty\tdirtyRead\tread\twrite\n");
        for(Iterator<Partition> i = partitions.iterator(); i.hasNext(); ) {
            Partition partition= i.next();
            partition.readwrite();
            dirty += partition.getDirty();
            dirtyRead += partition.getDirtyRead();
            num_read += partition.getNum_read();
            num_write += partition.getNum_write();
            System.out.printf("%d\t%d\t%d\t%d\n", partition.getDirty(), partition.getDirtyRead(), partition.getNum_read(), partition.getNum_write());
        }

        System.out.printf("%d\t%d\t%d\t%d\n", dirty, dirtyRead, num_read, num_write);

        System.out.print("lba\tdistance\n");
        for(Iterator<Partition> i = partitions.iterator(); i.hasNext(); ) {
            Partition partition = i.next();
            for(Iterator<Trace> j = partition.dirtyRead_list.iterator(); j.hasNext(); ) {
                Trace trace = j.next();
                System.out.printf("%d\t%d\n", trace.LBA(), trace.distance);
            }
        }
    }
}

