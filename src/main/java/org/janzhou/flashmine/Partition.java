package org.janzhou.flashmine;

import java.util.*;

/**
 * Created by Jian Zhou on 5/25/15.
 */

public class Partition {
    private List<Trace> traces =  new ArrayList<Trace>();
    private List<Trace> writes =  new ArrayList<Trace>();
    private int size = 250;

    public Partition() {
    }

    public Partition(int size) {
        this.size = size;
    }

    public boolean add(Trace trace) {
        if(traces.size() >= size) {
            return false;
        } else {
            trace.order = traces.size();
            traces.add(trace);
            return true;
        }
    }

    public int getSize() {
        return traces.size();
    }


    public int maxSize() {
        return size;
    }

    private int num_read = 0;
    private int num_write = 0;
    private int dirtyRead = 0;
    private int dirty = 0;

    public int getNum_read() {
        return num_read;
    }

    public int getNum_write() {
        return num_write;
    }

    public int getDirtyRead() {
        return dirtyRead;
    }

    public int getDirty() {
        return dirty;
    }

    public List<Trace> dirtyRead_list;

    public void readwrite() {
        dirtyRead_list =  new LinkedList<Trace>();

        for(Iterator<Trace> i = traces.iterator(); i.hasNext(); ) {
            Trace trace = i.next();
            if(!trace.isRead()) {
                num_write += 1;
                writes.add(trace);
            } else {
                num_read += 1;
            }
        }
        dirty = writes.size();

        for(Iterator<Trace> i = writes.iterator(); i.hasNext(); ) {
            Trace write = i.next();
            for(Iterator<Trace> j = traces.iterator(); j.hasNext(); ) {
                Trace read = j.next();
                if (read.LBA() == write.LBA() && read.isRead() && read.order > write.order) {
                    dirtyRead += 1;
                    Trace trace = read.clone();
                    trace.distance = read.order - write.order;
                    dirtyRead_list.add(trace);
                    break;
                }
            }
        }
    }

    public void print() {
        for(Iterator<Trace> i = traces.iterator(); i.hasNext(); ) {
            Trace trace = i.next();
            trace.print();
        }
    }

    public static List<Partition> fromTraces(List<Trace> traces, int size) {
        List<Partition> partitions = new LinkedList<Partition>();

        Partition partition = new Partition(size);

        for(Iterator<Trace> i = traces.iterator(); i.hasNext(); ) {
            Trace trace = i.next();
            if(partition.add(trace) == false) {
                partitions.add(partition);
                partition = new Partition(size);
                partition.add(trace);
            }
        }

        partitions.add(partition);

        return partitions;
    }

    public static List<Partition> fromFile(String file, int size) {
        List<Trace> traces = Trace.readFile(file);
        return Partition.fromTraces(traces, size);
    }

}
