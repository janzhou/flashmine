package org.janzhou.flashmine;

import java.io.*;
import java.util.*;

/**
 * Created by Jian Zhou on 5/25/15.
 */

public class Trace {
    public int order = 0; // order in Partition; used by ReadWrite Analysis
    public int distance = 0; // distance between dirty read; used by ReadWrite Analysis
    private int ASU;
    private int LBA;
    private int SIZE;
    private boolean READ;
    private double TIMESTEMP;

    public Trace(
            int ASU, int LBA, int SIZE, boolean READ, double TIMESTEMP
    ) {
        this.ASU = ASU;
        this.LBA = LBA;
        this.SIZE = SIZE;
        this.READ = READ;
        this.TIMESTEMP = TIMESTEMP;
    }

    public Trace(String trace_line){
        //System.out.println(trace_line);
        String[] trace = trace_line.split(",");
        assert(trace.length > 5);
        this.ASU = Integer.parseInt(trace[0]);
        this.LBA = Integer.parseInt(trace[1]);
        this.SIZE = Integer.parseInt(trace[2]);
        if("r".equals(trace[3]) || "R".equals(trace[3])) {
            this.READ = true;
        } else {
            this.READ = false;
        }
        this.TIMESTEMP = Double.parseDouble(trace[4]);
    }

    public Trace clone() {
        return new Trace(ASU, LBA, SIZE, READ,TIMESTEMP);
    }

    public boolean isRead(){
        return this.READ;
    }

    public int LBA(){
        return this.LBA;
    }

    public int SIZE(){
        return this.SIZE;
    }

    public void print(){
        System.out.println(this.LBA);
    }

    public static List<Trace> readFile(String file_name){
        List<Trace> traces = new LinkedList<Trace>();
        try {
            File file = new File(file_name);
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            String line = null;
            while ((line = br.readLine()) != null) {
                traces.add(new Trace(line));
            }
        } catch (IOException e) {
            System.err.format("IOException: %s%n", e);
        }
        return traces;
    }
}
