package org.janzhou.flashmine;

import apriori4j.*;
import java.util.*;
import org.janzhou.*;

/**
 * Created by jan on 6/5/15.
 */
public class Correlation {
    private static List<Transaction> TraceRangeTransactions(List<Trace> traces){
        List<Transaction> transactions = new ArrayList<Transaction>();
        int j = 0;
        Set<String> items = new HashSet<String>();

        for(Iterator<Trace> i = traces.iterator(); i.hasNext(); ) {
            Trace trace = i.next();
            if(trace.isRead()) {
                continue;
            }
            items.add(Integer.toString(trace.LBA()/50000));
            if(j == 100) {
                j = 0;
                transactions.add(new Transaction(items));
                items = new HashSet<String>();
            } else {
                j += 1;
            }
        }

        return transactions;
    }

    private static List<Transaction> TraceTransactions(List<Trace> traces){
        List<Transaction> transactions = new ArrayList<Transaction>();
        int j = 0;
        Set<String> items = new HashSet<String>();

        for(Iterator<Trace> i = traces.iterator(); i.hasNext(); ) {
            Trace trace = i.next();
            if(trace.isRead()) {
                continue;
            }
            items.add(Integer.toString(trace.LBA()));
            if(j == 100) {
                j = 0;
                transactions.add(new Transaction(items));
                items = new HashSet<String>();
            } else {
                j += 1;
            }
        }

        return transactions;
    }

    static void show(AnalysisResult result){
        Map<Integer, Set<FrequentItemSet>> frequent =  result.getFrequentItemSets();

        for (Map.Entry<Integer, Set<FrequentItemSet>> entry : frequent.entrySet()) {
            int key = entry.getKey();
            Set<FrequentItemSet> value = entry.getValue();

            for(FrequentItemSet frequentItemSet : value) {
                ItemSet itemSet = frequentItemSet.getItemSet();
                System.out.print(itemSet.toString());
                Double support  = frequentItemSet.getSupport();
                System.out.println(support);
            }
        }
    }

    public static void main(String [] args) {
        List<Trace> traces = Trace.readFile(args[0]);

        List<Transaction> transactions = TraceTransactions(traces);
        AprioriAlgorithm aprioriAlgorithm = new AprioriAlgorithm(Double.parseDouble(args[1]), Double.parseDouble(args[2]));

        AnalysisResult result = aprioriAlgorithm.analyze(transactions);

        show(result);
    }
}
