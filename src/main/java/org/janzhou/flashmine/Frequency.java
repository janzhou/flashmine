package org.janzhou.flashmine;

import java.io.*;
import java.util.*;

import org.janzhou.flashmine.*;

/**
 * Created by jan on 6/9/15.
 */
public class Frequency {
    private static final int MapEntryPerPage = 1024;
    private static final int PageSize = 4*1024;

    public Frequency(String[] args){
    }


    public static void main(String [] args) {
        Map<Integer, Integer> freq = new HashMap<Integer, Integer> () {
            @Override
            public Integer get(Object key) {
                return containsKey(key) ? super.get(key) : 0;
            }
        };

        List<Trace> traces = Trace.readFile(args[0]);
        int block_size = Integer.parseInt(args[1]);
        for(Iterator<Trace> i = traces.iterator(); i.hasNext(); ) {
            Trace trace = i.next();
            if(trace.isRead()) {
                continue;
            }
            freq.put(trace.LBA()/block_size, freq.get(trace.LBA()/block_size) + 1);
        }

        Set<Integer> keys = freq.keySet();
        for(int key : keys) {
            System.out.println("" + key + " " + freq.get(key));
        }
    }
}
