# FlashMine
FlashMine

# Download Traces

```bash
./trace.sh
```

# Compile Java

```bash
mvn package
```

# Read-Write Analysis

The Read-Write Analysis is the dirty read frequency analysis.

```bash
java -cp target/flashmine-1.0-SNAPSHOT.jar org.janzhou.flashmine.ReadWrite trace/Financial1.spc 1024
```

# Correlation

This command analysis the read correlation. If you want to do regional correlation analysis, you need to modify the line in ``Correlation.java``.

```java
List<Transaction> transactions = TraceTransactions(traces);
```

```java
List<Transaction> transactions = TraceRangeTransactions(traces);
```

The command line is:

```bash
java -cp target/flashmine-1.0-SNAPSHOT.jar org.janzhou.flashmine.Correlation trace/Financial1.spc minSupport minConfidence
```

**minSupport and minConfidence are the percentage of minmium support and confidence**
